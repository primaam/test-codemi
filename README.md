
Petunjuk penggunaan aplikasi:

1. install aplikasi terlebih dahulu
2. jika belum mempunyain aku, registrasi terlebih dahulu dengan memasukan username dan password yang diinginkan, lalu klik tombol 'REGISTRASI'
3. tunggu sampai dinyatakan sukses, lalu login dengan mengetik username dan password yang sudah diregistrasi
4. klik tombol 'LOGIN' dan tunggu hingga proses selesai
5. jika kita ingin menampilkan QR code milik kita, klik 'SHOW YOUR QR CODE' dan akan muncul QR Code milik kita
6. jika kita ingin men-scan QR code, arahkan QR code yang ke layar kamera dan tunggu beberapa saat
