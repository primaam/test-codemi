import React, {useEffect, useState} from 'react'
import {View, Text, TouchableOpacity, StyleSheet, Modal} from 'react-native'
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera'; 
import QRCode from 'react-native-qrcode-svg';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {useSelector, useDispatch} from 'react-redux'

function Scanner(){
    const [modalVisible, setModalVisible] = useState(false)
    const dispatch = useDispatch()
    const data =  useSelector((state) => state.auth.data)
    const profile = useSelector((state) => state.auth.dataProfile)
    const [username, setUsername] = useState('Tidak Dikenal')
    
    const onSuccess = e => {
       alert(`HALLO !! ${JSON.stringify(e)}`)
    } 

    useEffect(()=> {
        if(profile){
            setUsername(profile.username)
        }
    }, [profile]) 

    return(
        <View style={styles.container}>
            <View style={{marginBottom: hp('3%')}}>
                <Text style={styles.text}>HALO, {username.toUpperCase()}</Text>
            </View>
            <TouchableOpacity
                onPress={()=> setModalVisible(true)} 
                style={styles.button}>
                    <Text style={styles.text}>Show Your QR Code</Text>
            </TouchableOpacity>
            <QRCodeScanner
                style={{flex: 1, width: '100%'}}
                onRead={onSuccess} />
            <Modal
                animationType='fade'
                transparent={true}
                modalBackground='blur'
                visible={modalVisible}>
                    <View style={styles.modal}>
                        <QRCode
                            value={username}
                            size={300}
                        />
                    </View>   
            </Modal>
        </View> 
    )
}

export default Scanner

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        padding: wp('5%')
    },
    text:{
        textAlign: 'center',
        fontSize: wp('5%')
    },
    button:{
        backgroundColor: '#ddd', 
        height: hp('5%'), 
        width: wp('80%'), 
        justifyContent: 'center',
        borderRadius: 10,
        paddingVertical: hp('5%'),
        marginVertical: hp('1.5%')
    },
    modal: {
        height: hp('100%'),
        width: wp('100%'),
        alignItems: 'center',
        backgroundColor: 'rgba(100,100,100, 0.5)',
        justifyContent: 'center',
        alignContent: 'center',
    }, 
})