import React, {useState} from 'react'
import {View, Text, StyleSheet, TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from 'react-native'
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    } from 'react-native-responsive-screen';
import {useSelector, useDispatch} from 'react-redux'

function Login(props){
    const [username, setUsername] = useState()
    const [password, setPassword] = useState()
    const dispatch = useDispatch()
    
    const loginHandler = () => {
        const data = {
            username,
            password
        }
        dispatch({type : 'LOGIN' , payload : data})
    }

    const registerHandler = () => {
        const data={
            username,
            password
        }
        dispatch({type: 'REGISTER', payload: data})
    }

    return (
        <TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
            <View style={styles.container}>
                <Text style={styles.text}>Welcome Back</Text>
                <Text style={styles.text}>QR Code Scanner</Text>
                <TextInput
                value={username}
                onChangeText={(text) => setUsername(text)} 
                placeholder='Username'
                style={styles.credential}/>
                <TextInput
                value={password}
                onChangeText={(text) => setPassword(text)}
                placeholder='Password'
                secureTextEntry={true}
                style={styles.credential}/>
                <View style={{flexDirection:'row',width: wp('70%'), justifyContent:'space-between'}}>
                    <TouchableOpacity style={styles.button}
                    onPress={() => loginHandler()}> 
                        <Text style={styles.text}>LOGIN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}
                    onPress={() => registerHandler()}> 
                        <Text style={styles.text}>REGISTER</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

export default Login


const styles = StyleSheet.create({
    container : {
        height: '100%',
        paddingHorizontal: wp('5%'),
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center'
    },
    credential:{
        marginVertical: hp('1.5%'),
        paddingVertical: hp('1%'),
        paddingHorizontal: wp('2.5%'),
        borderWidth: 1,
        borderRadius: 20,
        width: wp('90%')
    },
    button:{
        backgroundColor: '#ddd', 
        height: hp('5%'),
        paddingHorizontal: wp('3%'),  
        justifyContent: 'center',
        borderRadius: 10
    },
    text:{
        textAlign: 'center',
        fontSize: wp('5%')
    }
})
