// REDUCER
import {combineReducers} from 'redux'

const initialState = {
    isLoading : false,
    isLoggedIn : false,
    dataProfile: null,
    data: null
}

const auth = (state = initialState, action) => {
    switch(action.type) {
        case 'LOGIN': {
            return {
                ...state,
                isLoading: true,
            }
        }
        case 'LOGIN_SUCCESS' : {
            return {
                ...state,
                isLoading: false,
                isLoggedIn: true,
                data: action.payload
            }
        }
        case 'LOGIN_FAILED' : {
            return {
                ...state,
                isLoading: false,
                isLoggedIn: false,
            }
        }
        case 'REGISTER': {
            return {
                ...state,
                isLoading: true,
            }
        }
        case 'REGISTER_SUCCESS' : {
            return {
                ...state,
                isLoading: false,
            }
        }
        case 'REGISTER_FAILED' : {
            return {
                ...state,
                isLoading: false,
            }
        }
        case 'PROFILE': {
            return {
                ...state,
                isLoading: true,
            }
        }
        case 'PROFILE_SUCCESS' : {
            return {
                ...state,
                isLoading: false,
                dataProfile: action.payload
            }
        }
        case 'PROFILE_FAILED' : {
            return {
                ...state,
                isLoading: false,
            }
        }
        default: 
            return state
    }
}


export default combineReducers({
    auth
})