import axios from 'axios'

// MAIN API
const baseUrl = 'https://codemi-prima.herokuapp.com/'

export function apiGetUser(id, headers) {
    return axios ({
        method: 'GET',
        url: baseUrl + 'user/' + id,
        headers
    });
}

export function apiLogin(payload) {
    return axios ({
        method: 'POST',
        url: baseUrl + 'login',
        data: payload
    });
}

export function apiRegister(payload) {
    return axios ({
        method: 'POST',
        url: baseUrl + 'register',
        data: payload
    });
}
