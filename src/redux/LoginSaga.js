import {takeLatest, put, all} from 'redux-saga/effects'
import {apiLogin,apiRegister, apiGetUser} from '../redux/mainApi'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { ToastAndroid } from 'react-native'
import axios from 'axios'
const baseUrl = 'https://codemi-prima.herokuapp.com/'

// ASYNCSTORAGE
async function getHeaders() {
    const token = await  AsyncStorage.getItem('APP_TOKEN')
    return {
        'Content-Type' : 'application/json',
        Authorization: 'Bearer ' + token
    }
}

async function saveToken(token) {
    AsyncStorage.setItem('APP_TOKEN', token)
}


function* login(action){
    try{
        const resLogin = yield apiLogin(action.payload)
        
        if(resLogin && resLogin.data){
            yield saveToken(resLogin.data.token)
            yield put({type: 'LOGIN_SUCCESS', payload: resLogin.data})
            yield put({type:'PROFILE', payload: resLogin.data.id_user })
        }
    } catch {
        yield put ({type: 'LOGIN_FAILED'})
    }
    
}

function* register(action){
    try{
        const resRegister = yield apiRegister(action.payload)
        
        if (resRegister && resRegister.data){
            yield saveToken(resRegister.data.token)
            yield put ({type: 'REGISTER_SUCCESS'})
           
        }
        ToastAndroid.showWithGravity(
            'Register Sukses, Silahkan Login',
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
        );
    }catch(e) {
        console.log('ini error', e)
        yield put({type: 'REGISTER_FAILED'})
    }
}

function* profile(action){
    try{
        const headers= yield getHeaders()
        const resProfile = yield apiGetUser(action.payload, headers)
        yield put({type: 'PROFILE_SUCCESS', payload: resProfile.data[0]})
    } catch(e) {
        yield put ({type: 'PROFILE_FAILED'})
        console.log('ini error profile', e)
    }
    
}

function* authSaga(){
    yield takeLatest('LOGIN', login)
    yield takeLatest('REGISTER', register)
    yield takeLatest('PROFILE', profile)
}

export default function* rootSaga(){
    yield all([authSaga()])
}
