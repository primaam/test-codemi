import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import Login from '../screen/Login'
import Scanner from '../screen/Scanner'
import {useSelector} from 'react-redux'

const Stack = createStackNavigator();

function AppStack(props){
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
    
    return(
        <NavigationContainer>
            <Stack.Navigator>
                {isLoggedIn ? (
                    <Stack.Screen
                        options={{headerShown: false}}
                        name='scanner'
                        component={Scanner}
                    />
                ):(
                    <Stack.Screen
                        options={{headerShown: false}}
                        name='login'
                        component={Login}
                    />
                )}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppStack